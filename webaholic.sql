-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2016 at 02:13 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webaholic`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers_auth`
--

CREATE TABLE IF NOT EXISTS `customers_auth` (
`uid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers_auth`
--

INSERT INTO `customers_auth` (`uid`, `name`, `email`, `phone`, `password`, `address`, `city`, `created`) VALUES
(169, 'Swadesh Behera', 'swadesh@gmail.com', '1234567890', '$2a$10$251b3c3d020155f7553c1ugKfEH04BD6nbCbo78AIDVOqS3GVYQ46', '4092 Furth Circle', 'Singapore', '2014-08-31 12:51:20'),
(170, 'Ipsita Sahoo', 'ipsita@gmail.com', '1111111111', '$2a$10$d84ffcf46967db4e1718buENHT7GVpcC7FfbSqCLUJDkKPg4RcgV2', '2, rue du Commerce', 'NYC', '2014-08-31 13:00:58'),
(171, 'Trisha Tamanna Priyadarsini', 'trisha@gmail.com', '2222222222', '$2a$10$c9b32f5baa3315554bffcuWfjiXNhO1Rn4hVxMXyJHJaesNHL9U/O', 'C/ Moralzarzal, 86', 'Burlingame', '2014-08-31 13:02:03'),
(172, 'Sai Rimsha', 'rimsha@gmail.com', '3333333333', '$2a$10$477f7567571278c17ebdees5xCunwKISQaG8zkKhvfE5dYem5sTey', '897 Long Airport Avenue', 'Madrid', '2014-08-31 15:04:21'),
(173, 'Satwik Mohanty', 'satwik@gmail.com', '4444444444', '$2a$10$2b957be577db7727fed13O2QmHMd9LoEUjioYe.zkXP5lqBumI6Dy', 'Lyonerstr. 34', 'San Francisco\n', '2014-08-31 15:06:02'),
(174, 'Tapaswini Sahoo', 'linky@gmail.com', '5555555555', '$2a$10$b2f3694f56fdb5b5c9ebeulMJTSx2Iv6ayQR0GUAcDsn0Jdn4c1we', 'ul. Filtrowa 68', 'Warszawa', '2014-08-31 15:14:54'),
(175, 'Manas Ranjan Subudhi', 'manas@gmail.com', '6666666666', '$2a$10$03ab40438bbddb67d4f13Odrzs6Rwr92xKEYDbOO7IXO8YvBaOmlq', '5677 Strong St.', 'Stavern\n', '2014-08-31 15:15:08'),
(178, 'AngularCode Administrator', 'admin@angularcode.com', '0000000000', '$2a$10$72442f3d7ad44bcf1432cuAAZAURj9dtXhEMBQXMn9C8SpnZjmK1S', 'C/1052, Bangalore', '', '2014-08-31 15:30:26'),
(187, 'Prabhdeep', 'pr@gmail.com', '9090909090909', '$2a$10$a22d258f2c331fa9e201aewxbs3jkzSifJ0H5yKxuvyRBJFFP4ceG', 'opoop', '', '2016-04-07 11:47:59');

-- --------------------------------------------------------

--
-- Table structure for table `web_administrators`
--

CREATE TABLE IF NOT EXISTS `web_administrators` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_clients`
--

CREATE TABLE IF NOT EXISTS `web_clients` (
`id` int(11) NOT NULL,
  `unique_key` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_clients`
--

INSERT INTO `web_clients` (`id`, `unique_key`, `username`, `password`, `first_name`, `last_name`, `email`, `domain`, `company_name`, `role`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, '570bd1e26d270 ', 'Raman009', '$2a$10$a22d258f2c331fa9e201aewxbs3jkzSifJ0H5yKxuvyRBJFFP4ceG', 'Raman', 'Bhaskar', 'rm@gmail.com', 'https://www.xyz.com', 'Raman Enterprices.', 1, '2016-04-08 16:26:23', '0000-00-00 00:00:00', NULL),
(4, '570bd2046f7a1 ', 'Raman1', '$2a$10$a22d258f2c331fa9e201aewxbs3jkzSifJ0H5yKxuvyRBJFFP4ceG', 'Raman1', 'Bhaskar1', 'rammmm@gmail.com', 'https://www.abc.com', 'XVZ...', 2, '2016-04-08 16:26:23', '0000-00-00 00:00:00', NULL),
(5, '570ce2d06c850', 'RamanBWC', NULL, 'ram', 'Bhasker', 'rb@gmail.com', 'rrrb.com', 'RRB.org', 1, '2016-04-12 06:28:08', '2016-04-12 11:58:08', NULL),
(6, '570d26729837b', 'SteveP', NULL, 'Steve', 'P', 'steve@gmail.com', 'steve.com', 'steve.org', 1, '2016-04-12 11:16:42', '2016-04-12 16:46:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_clients_projects`
--

CREATE TABLE IF NOT EXISTS `web_clients_projects` (
`id` int(11) NOT NULL,
  `client_id` int(255) DEFAULT NULL,
  `unique_key` varchar(255) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_description` varchar(255) DEFAULT NULL,
  `project_url` varchar(255) DEFAULT NULL,
  `project_type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_clients_projects`
--

INSERT INTO `web_clients_projects` (`id`, `client_id`, `unique_key`, `project_name`, `project_description`, `project_url`, `project_type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '', 'XYZ', 'xxxxxxxxx', 'http://XYZ.com', '', 1, '2016-04-08 17:01:55', '0000-00-00 00:00:00', NULL),
(4, 3, '', 'dgdg', NULL, 'dfgdfg', 'dfgdfg', 1, '2016-04-11 07:07:04', '2016-04-11 12:37:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_clients_projects_analytic_competitors`
--

CREATE TABLE IF NOT EXISTS `web_clients_projects_analytic_competitors` (
`id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  `competitor_url` varchar(255) DEFAULT NULL,
  `organic_rank` varchar(255) DEFAULT NULL,
  `organic_rank_changes` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_clients_projects_analytic_keywords`
--

CREATE TABLE IF NOT EXISTS `web_clients_projects_analytic_keywords` (
`id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_clients_projects_tasks`
--

CREATE TABLE IF NOT EXISTS `web_clients_projects_tasks` (
`id` int(11) NOT NULL,
  `cleint_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `task_desc` text,
  `task_members` varchar(500) NOT NULL,
  `due_date` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `last_updated` varchar(255) NOT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_roles`
--

CREATE TABLE IF NOT EXISTS `web_roles` (
`id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers_auth`
--
ALTER TABLE `customers_auth`
 ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `web_administrators`
--
ALTER TABLE `web_administrators`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_clients`
--
ALTER TABLE `web_clients`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_clients_projects`
--
ALTER TABLE `web_clients_projects`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_clients_projects_analytic_competitors`
--
ALTER TABLE `web_clients_projects_analytic_competitors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_clients_projects_analytic_keywords`
--
ALTER TABLE `web_clients_projects_analytic_keywords`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_clients_projects_tasks`
--
ALTER TABLE `web_clients_projects_tasks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_roles`
--
ALTER TABLE `web_roles`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers_auth`
--
ALTER TABLE `customers_auth`
MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `web_administrators`
--
ALTER TABLE `web_administrators`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_clients`
--
ALTER TABLE `web_clients`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `web_clients_projects`
--
ALTER TABLE `web_clients_projects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `web_clients_projects_analytic_competitors`
--
ALTER TABLE `web_clients_projects_analytic_competitors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_clients_projects_analytic_keywords`
--
ALTER TABLE `web_clients_projects_analytic_keywords`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_clients_projects_tasks`
--
ALTER TABLE `web_clients_projects_tasks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_roles`
--
ALTER TABLE `web_roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
