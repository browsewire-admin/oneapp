<!DOCTYPE html>
<html lang="en" ng-app="myApp">

  <head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width,initial-scale=1">
			<title>Login | Webaholic</title>
			<!-- Bootstrap -->
			<link href="loginSection/loginCss/css/bootstrap.min.css" rel="stylesheet">
			<link href="loginSection/loginCss/css/custom.css" rel="stylesheet">
			<link href="loginSection/loginCss/css/toaster.css" rel="stylesheet">
			<style>
			a {
			color: orange;
			}
			</style>
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
			<!-- END GLOBAL MANDATORY STYLES -->
			<!-- BEGIN PAGE LEVEL STYLES -->
			<link href="http://localhost:8080/assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/css/login1.css" rel="stylesheet" type="text/css"/>
			<style>
			.login-header{
				
				background: none repeat scroll 0 0 #222533;
				border-bottom: 1px solid #CCCCCC;
				color: #FFFFFF;
				font-size: 33px;
				font-weight: bold;
				padding-bottom: 20px;
				padding-top: 20px;
				text-align: center;
				width: 100%;
			}
			.login .logo {
			    margin: 0 !important;
			    
			}	
			</style>	

			<!--link href="http://gamtes.com/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/-->
			<!-- END PAGE LEVEL SCRIPTS -->
			<!-- BEGIN THEME STYLES -->
			<link href="http://localhost:8080/assets/global/css/components.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
			<link id="style_color" href="http://localhost:8080/assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
			<link href="http://localhost:8080/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
			<!-- END THEME STYLES -->
			<link rel="shortcut icon" href="favicon.ico"/>
 </head>

  <body ng-cloak="" class="login">
    
      
        <div data-ng-view="" id="ng-view" class="slide-animation"></div>

      
    </body>
  <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
  <!-- Libs -->


	<script src="http://localhost/barberShop/public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/jquery-migrate-1.2.1.min.js"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/jquery.blockui.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/jquery.cokie.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<!-- END CORE PLUGINS -->


	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
	<script src="http://localhost/barberShop/public/assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/global/scripts/metronic.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/admin/layout/scripts/layout.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/admin/layout/scripts/quick-sidebar.js"></script>
	<script type="text/javascript" src="http://localhost/barberShop/public/assets/admin/pages/scripts/login.js"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {  
			   
			Metronic.init(); // init metronic core components
			Layout.init(); // init current layout
			QuickSidebar.init() // init quick sidebar
			Login.init();
			
		});
		</script>
	<!-- END JAVASCRIPTS -->
	<script>
	$('#cancel').click(function(){
		
		return history.go(-1);
		
	});
	</script>	
	<script>
	var nVer = navigator.appVersion;
	var nAgt = navigator.userAgent;
	var browserName  = navigator.appName;
	var fullVersion  = ''+parseFloat(navigator.appVersion); 
	var majorVersion = parseInt(navigator.appVersion,10);
	var nameOffset,verOffset,ix;

	// In Opera 15+, the true version is after "OPR/" 
	if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
	 browserName = "Opera";
	 fullVersion = nAgt.substring(verOffset+4);
	}
	// In older Opera, the true version is after "Opera" or after "Version"
	else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
	 browserName = "Opera";
	 fullVersion = nAgt.substring(verOffset+6);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In MSIE, the true version is after "MSIE" in userAgent
	else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
	 browserName = "Microsoft Internet Explorer";
	 fullVersion = nAgt.substring(verOffset+5);
	}
	// In Chrome, the true version is after "Chrome" 
	else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
	 browserName = "Chrome";
	 fullVersion = nAgt.substring(verOffset+7);
	}
	// In Safari, the true version is after "Safari" or after "Version" 
	else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
	 browserName = "Safari";
	 fullVersion = nAgt.substring(verOffset+7);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In Firefox, the true version is after "Firefox" 
	else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
	 browserName = "Firefox";
	 fullVersion = nAgt.substring(verOffset+8);
	}
	// In most other browsers, "name/version" is at the end of userAgent 
	else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
	          (verOffset=nAgt.lastIndexOf('/')) ) 
	{
	 browserName = nAgt.substring(nameOffset,verOffset);
	 fullVersion = nAgt.substring(verOffset+1);
	 if (browserName.toLowerCase()==browserName.toUpperCase()) {
	  browserName = navigator.appName;
	 }
	}
	// trim the fullVersion string at semicolon/space if present
	if ((ix=fullVersion.indexOf(";"))!=-1)
	   fullVersion=fullVersion.substring(0,ix);
	if ((ix=fullVersion.indexOf(" "))!=-1)
	   fullVersion=fullVersion.substring(0,ix);

	majorVersion = parseInt(''+fullVersion,10);
	if (isNaN(majorVersion)) {
	 fullVersion  = ''+parseFloat(navigator.appVersion); 
	 majorVersion = parseInt(navigator.appVersion,10);
	}
	if(browserName == "Safari") {

		$('select[name=role]').css('text-indent','22px');

	}

	</script>

  <script src="loginSection/loginJs/js/angular.min.js"></script>
  <script src="loginSection/loginJs/js/angular-route.min.js"></script>
  <script src="loginSection/loginJs/js/angular-animate.min.js" ></script>
  <script src="loginSection/loginJs/js/toaster.js"></script>
  <script src="loginSection/loginJs/app/app.js"></script>
  <script src="loginSection/loginJs/app/data.js"></script>
  <script src="loginSection/loginJs/app/directives.js"></script>
  <script src="loginSection/loginJs/app/authCtrl.js"></script> 
</html>

