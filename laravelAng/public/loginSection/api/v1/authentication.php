<?php 
$app->get('/session', function() {
    $db = new DbHandler();
    $session = $db->getSession();
    $response["id"] = $session['id'];
    $response["email"] = $session['email'];
    $response["first_name"] = $session['first_name'];
    $response["role"] = $session['role'];
    echoResponse(200, $session);
});

$app->post('/login', function() use ($app) {
    require_once 'passwordHash.php';
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('email', 'password'),$r->customer);
    $response = array();
    $db = new DbHandler();
    $password = $r->customer->password;
    $email = $r->customer->email;
    
    //$user = $db->getOneRecord("select id,first_name,password,email from web_clients where phone='$email' or email='$email'");
    $user = $db->getOneRecord("select id,first_name,password,email,role from web_clients where email='$email'");
    if ($user != NULL) {
        if(passwordHash::check_password($user['password'],$password)){
        $response['status'] = "success";
        $response['message'] = 'Logged in successfully.';
        $response['first_name'] = $user['first_name'];
        $response['id'] = $user['id'];
        $response['email'] = $user['email'];
        $response['role'] = $user['role'];
        //$response['createdAt'] = $user['created'];
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['id'] = $user['id'];
        $_SESSION['email'] = $email;
        $_SESSION['first_name'] = $user['first_name'];
        $_SESSION['role'] = $user['role'];
        } else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
    }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
    echoResponse(200, $response);
});
$app->post('/signUp', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('email', 'first_name', 'password'),$r->customer);
    require_once 'passwordHash.php';
    $db = new DbHandler();
    $phone = $r->customer->phone;
    $first_name = $r->customer->first_name;
    $email = $r->customer->email;
    $address = $r->customer->address;
    $password = $r->customer->password;
    $role = $r->customer->role;
    //$isUserExists = $db->getOneRecord("select 1 from web_clients where phone='$phone' or email='$email'");
    $isUserExists = $db->getOneRecord("select 1 from web_clients where  email='$email'");
    if(!$isUserExists){
        $r->customer->password = passwordHash::hash($password);
        $tabble_first_name = "web_clients";
        //$column_first_names = array('phone', 'first_name', 'email', 'password', 'city', 'address');
        $column_first_names = array('first_name', 'email', 'password', 'city', 'address');
        $result = $db->insertIntoTable($r->customer, $column_first_names, $tabble_first_name);
        if ($result != NULL) {
            $response["status"] = "success";
            $response["message"] = "User account created successfully";
            $response["id"] = $result;
            if (!isset($_SESSION)) {
                session_start();
            }
            $_SESSION['id'] = $response["id"];
            //$_SESSION['phone'] = $phone;
            $_SESSION['first_name'] = $first_name;
            $_SESSION['email'] = $email;
           	$_SESSION['role'] = $role;
            
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create customer. Please try again";
            echoResponse(201, $response);
        }            
    }else{
        $response["status"] = "error";
        $response["message"] = "An user with the provided phone or email exists!";
        echoResponse(201, $response);
    }
});
$app->get('/logout', function() {
    $db = new DbHandler();
    $session = $db->destroySession();
    $response["status"] = "info";
    $response["message"] = "Logged out successfully";
    echoResponse(200, $response);
});
?>