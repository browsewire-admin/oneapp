'use strict';

MetronicApp.controller('DashboardController', function($rootScope, $scope, $http, $timeout) {
    
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        Metronic.initAjax();
        //$scope.example = "hello";
    });
    $http({ 
	       method: 'GET', 
	       url: '/loginSection/api/v1/session', 
	       params: {}
			}).success(function(response) {
				  
				  console.log(response.first_name);

				  if(response.first_name == "Guest")
				  {
			  			$rootScope.authenticated = true;
			  			 $scope.username = response.first_name;
			  			if(response.role == "")
			  			{
		  						window.location.href = "http://localhost:8080/login";

			  			}else {

			  					window.location.href = "http://localhost:8080/dashboard";

			  			}

			 		}
				   		
			}).error(function(error) {
		      
   	});
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageSidebarClosed = false;
});