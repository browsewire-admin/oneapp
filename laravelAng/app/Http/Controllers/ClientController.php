<?php namespace Todo\Http\Controllers;

use Illuminate\Http\Request;
use Todo\Http\Requests;
use Todo\Todo;
use Todo\Client;
use DB;


class ClientController extends Controller
{
  public $_allow = array();
	public $_content_type = "application/json";
	public $_request = array();
	
	public $_method = "";		
	public $_code = 200;  

	public function get_request_method()
	{

		return $_SERVER['REQUEST_METHOD'];
	}
	public function get_status_message(){
		$status = array(
						200 => 'OK',
						201 => 'Created',  
						204 => 'No Content',  
						404 => 'Not Found',  
						406 => 'Not Acceptable');
		return ($status[$this->_code])?$status[$this->_code]:$status[500];
	}
	public function set_headers(){
			header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
			header("Content-Type:".$this->_content_type);
		}

	public function response($data,$status)
	{

			$this->_code = ($status)?$status:200;
			$this->set_headers();
			echo $data;
			exit;
		}

  public function insertClient(Request $request)
	{
		$unique_id = uniqid();
		$clients = $request->only('unique_key','username','first_name','last_name','email','domain','company_name');
		$store["country_id"] = $unique_id;
	
		if(Client::create($clients)) {

			$success = array('status' => "Success", "msg" => "Client Created Successfully.", "data" => $clients);
			$this->response(json_encode($success),200);

		}else{

			$this->response('',204);	//"No Content" status

		}
		
	}

	public function allClients()
	{	
		
		return Client::all();
		// if($this->get_request_method() != "GET"){
		// 	$this->response('',406);
		// }
		// $query="SELECT distinct c.customerNumber, c.customerName, c.email, c.address, c.city, c.state, c.postalCode, c.country FROM angularcode_customers c order by c.customerNumber desc";
		// $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

		// if($r->num_rows > 0)
		// {
		// 	$result = array();
		// 	while($row = $r->fetch_assoc()){
		// 		$result[] = $row;
		// 	}
		// 	$this->response(json_encode($result), 200); // send user details

		// }else{


		// }
		// $this->response('',204);	// If no records "No Content" status
	}

	function getClientList()
	{
		//get all clients
		return	$allClients =  Client::all();
    //->lists('client_name', 'id');
		


	}


}
