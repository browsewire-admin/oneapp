<?php namespace Todo\Http\Controllers;

use Illuminate\Http\Request;
use Todo\Http\Requests;
use Todo\Todo;
use Todo\Project;


class ProjectController extends Controller
{
  public $_allow = array();
	public $_content_type = "application/json";
	public $_request = array();
	
	public $_method = "";		
	public $_code = 200;

	public function get_request_method()
	{

		return $_SERVER['REQUEST_METHOD'];
	}
	public function get_status_message(){
		$status = array(
						200 => 'OK',
						201 => 'Created',  
						204 => 'No Content',  
						404 => 'Not Found',  
						406 => 'Not Acceptable');
		return ($status[$this->_code])?$status[$this->_code]:$status[500];
	}
	public function set_headers(){
			header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
			header("Content-Type:".$this->_content_type);
		}

	public function response($data,$status)
	{

			$this->_code = ($status)?$status:200;
			$this->set_headers();
			echo $data;
			exit;
		}
	public function allProjects()
	{	
		
		return Project::all();
		
	}

	public function insertProject(Request $request)
	{
		
		$projects = $request->only('project_name','project_type','project_description','project_url','client_id');
	
		if(Project::create($projects)) {

			$success = array('status' => "Success", "msg" => "Project Created Successfully.", "data" => $projects);
			$this->response(json_encode($success),200);

		}else{

			$this->response('',204);	//"No Content" status

		}
		
	}


}
