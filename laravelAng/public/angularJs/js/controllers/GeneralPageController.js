'use strict';

MetronicApp.controller('GeneralPageController', function($rootScope, $scope, $http, $timeout,$location) {
    
    $scope.toggle = true;
    $scope.saveClient = function(client) {
    	//console.log(client.username);
    	if(client.username !=""){
    		//$location.path('/addClient');
    		$http({ 
		       method: 'POST', 
		       url: '/insertClient', 
		       params: {
		       		username:$scope.client.username,
		          first_name:$scope.client.first_name,
		          last_name:$scope.client.last_name,
		          email:$scope.client.email,
		          domain:$scope.client.domain,
		          company_name:$scope.client.company_name,
		          
		       }
		   
				   }).success(function(response){
				   		console.log("success");
				   		location.reload();
				       //window.location.replace("#/dashboard");
				      // $location.path("/addClient");
				   }).error(function(error){
				      //
				   });

    	}
    	
      	
    };

    //get all the clients
    
    $http({ 
	     method: 'GET', 
	     url: '/allClients', 
	   }).success(function(response) {

	   		 $scope.clients = response;
	   		 //console.log($scope.clients);

	   }).error(function(error){
		      //
		 });

		 //get all the clients
    
    $http({ 
	     method: 'GET', 
	     url: '/allProjects', 
	   }).success(function(response) {

	   		 $scope.projects = response;
	   		 //console.log($scope.clients);

	   }).error(function(error){
		      //
		 });
    
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        Metronic.initAjax();
        //$scope.example = "hello";
    });
    // TableAdvanced.init();
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageSidebarClosed = false;
});


